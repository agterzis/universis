import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachmentTypesTableComponent } from './components/attachment-types-table/attachment-types-table.component';
import { AttachmentTypesRootComponent } from './components/attachment-types-root/attachment-types-root.component';
import { AttachmentTypesHomeComponent } from './components/attachment-types-home/attachment-types-home.component';
import {AttachmentTypesRoutingModule} from './attachment-types.routing';
import {AttachmentTypesSharedModule} from './attachment-types.shared';
import {TablesModule} from '@universis/ngx-tables';
import {RouterModalModule} from '@universis/common/routing';
import {MostModule} from '@themost/angular';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {RequestsModule} from '../requests/requests.module';
import {SharedModule} from '@universis/common';
import {AttachmentTypeAdvancedTableSearchComponent} from './components/attachment-types-table/attachment-types-advanced-table-search.component';
import {FormsModule} from '@angular/forms';
import { AttachmentTypesOverviewComponent } from './components/attachment-types-dashboard/attachment-types-overview/attachment-types-overview.component';
import { AttachmentTypesDashboardComponent } from './components/attachment-types-dashboard/attachment-types-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    AttachmentTypesRoutingModule,
    AttachmentTypesSharedModule,
    TablesModule,
    RouterModalModule,
    MostModule,
    RegistrarSharedModule,
    TranslateModule,
    RequestsModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    AttachmentTypesTableComponent,
    AttachmentTypesRootComponent,
    AttachmentTypesHomeComponent,
    AttachmentTypeAdvancedTableSearchComponent,
    AttachmentTypesOverviewComponent,
    AttachmentTypesDashboardComponent
  ],
  exports: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AttachmentTypesModule { }
