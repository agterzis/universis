import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesFormComponent } from './classes-form.component';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import {TestingConfigurationService} from '../../../../test';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';

describe('ClassesFormComponent', () => {
  let component: ClassesFormComponent;
  let fixture: ComponentFixture<ClassesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClassesFormComponent
      ],
      imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot([]),
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
