import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-exams',
  templateUrl: './courses-overview-exams.component.html',
  styleUrls: ['./courses-overview-exams.component.scss']
})
export class CoursesOverviewExamsComponent implements OnInit, OnDestroy {
  public model: any;
  public grading: any;
  @Input() currentYear: any;
  public courseId: any;
  private subscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this.model = await this._context.model('CourseExams')
        .where('course').equal(this.courseId)
        .expand('examPeriod,status,completedByUser,year,course($expand=department)')
        .and('year').equal({'$name': '$it/course/department/currentYear'})
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
