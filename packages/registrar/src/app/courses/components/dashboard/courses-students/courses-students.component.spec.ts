import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesStudentsComponent } from './courses-students.component';

describe('CoursesStudentsComponent', () => {
  let component: CoursesStudentsComponent;
  let fixture: ComponentFixture<CoursesStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
