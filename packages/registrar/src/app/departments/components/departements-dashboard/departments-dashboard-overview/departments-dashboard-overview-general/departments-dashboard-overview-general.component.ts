import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-departments-dashboard-overview-general',
  templateUrl: './departments-dashboard-overview-general.component.html',
})
export class DepartmentsDashboardOverviewGeneralComponent  implements OnInit {
  public departmentsId: any;
  public department: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this.departmentsId = this._activatedRoute.snapshot.params.id;

    this.department = await this._context.model('LocalDepartments')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
  }
}
