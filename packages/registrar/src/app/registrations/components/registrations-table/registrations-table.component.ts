import {Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import * as REGISTRATIONS_LIST_CONFIG from './registrations-table.config.list.json';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { ActivatedRoute } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import { TranslateService } from '@ngx-translate/core';
import {AppEventService, ErrorService, LoadingService, ModalService} from '@universis/common';
import {AngularDataContext} from '@themost/angular';
import {ClientDataQueryable} from '@themost/client';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-registrations-table',
  templateUrl: './registrations-table.component.html',
  styleUrls: ['./registrations-table.component.scss']
})
export class RegistrationsTableComponent implements OnInit, OnDestroy  {


  public readonly config = REGISTRATIONS_LIST_CONFIG;
  public recordsTotal: any;
  private selectedItems: any;
  private dataSubscription: Subscription;
  private _batchSize = 100;
  public activeDepartment: any;
  private studentsWithoutRegistration = []; // hold students without registration (current year/period)
  private searchResults: any;
  private studentsFirstLoad = true;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _activeDepartment: ActiveDepartmentService,
    private _appEvent: AppEventService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(async data => {
      this._appEvent.change.next({
        model: 'ToggleShowExport',
        target: true
      });
      this._activatedTable.activeTable = this.table;
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        // get active department
        this.activeDepartment = await this._activeDepartment.getActiveDepartment();
        // assign id to form
        Object.assign(this.search.form, { department: this.activeDepartment && this.activeDepartment.id });
        this.search.ngOnInit();
      }
      if (data.tableConfiguration) {
        // ensure table is scrollable
        this.table.scrollable = true;
        this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
        // use custom method for getting students without registration
        if (this.table.config && this.table.config.model === 'Students') {
          this._appEvent.change.next({
            model: 'ToggleShowExport',
            target: false
          });
          this.recordsTotal = null;
          this.studentsFirstLoad = true;
          this.searchResults = null;
          // show loading
          this._loadingService.showLoading();
          // disable scroll on table
          this.table.scrollable = false;
          try {
            // fetch for first load
            this.studentsWithoutRegistration = (await this.fetchAll(true)).filter(student => !(student.registrations
              && student.registrations.length && student.registrations[0].hasCurrentRegistration));
          } catch (err) {
            console.error(err);
          } finally {
            this._loadingService.hideLoading();
          }
        }
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });

  }

  async onDataLoad(data: AdvancedTableDataResult) {
    try {
      this.recordsTotal = data.recordsTotal;
      if (this.table.config && this.table.config.model === 'Students') {
        if (this.studentsFirstLoad) {
          // set data
          data.data = this.searchResults || this.studentsWithoutRegistration;
          // set data records total
          data.recordsTotal = (this.searchResults && this.searchResults.length != null)
            ? this.searchResults.length
            : this.studentsWithoutRegistration.length;
          data.recordsFiltered = (this.searchResults && this.searchResults.length != null)
            ? this.searchResults.length
            : this.studentsWithoutRegistration.length;
          // set records total
          this.recordsTotal = (this.searchResults && this.searchResults.length != null)
            ? this.searchResults.length
            : this.studentsWithoutRegistration.length;
          // handle no items message
          if (this.recordsTotal === 0) {
            this.table.dataTable.context[0].oLanguage.sEmptyTable = this.table.emptyTable.nativeElement.innerHTML;
          }
          // toggle first load
          this.studentsFirstLoad = false;
          // reset search results
          this.searchResults = null;
          this._loadingService.hideLoading();
        } else {
          this._loadingService.showLoading();
          // reset data
          data.data = null;
          data.recordsTotal = null;
          data.recordsFiltered = null;
          this.recordsTotal = null;
          // fetch with first load false
          this.searchResults = (await this.fetchAll(false)).filter(student => !(student.registrations
            && student.registrations.length && student.registrations[0].hasCurrentRegistration));
          // set first load
          this.studentsFirstLoad = true;
          // reload
          this.table.fetch(true);
        }
      }
    } catch (err) {
      console.error(err);
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  /*private async takeSelectedItems(take?: number, skip?: number) {
    let items = {
      total: 0,
      skip: 0,
      value: []
    };
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          let smartTake = -1;
          let smartSkip = 0;
          if (typeof take === 'number') {
            smartTake = take;
          }
          if (typeof skip === 'number') {
            smartSkip = skip;
          }
          // get items
          const selectArguments = ['id',
              'status/alternateName as status',
              'registrationYear/id as registrationYear',
              'registrationPeriod/id as registrationPeriod'
          ];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(smartTake)
              .skip(smartSkip)
              .getList();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = {
              total: queryItems.total - this.table.unselected.length,
              skip: smartSkip,
              value: queryItems.value.filter( item => {
                return this.table.unselected.findIndex( (x) => {
                  return x.id === item.id;
                }) < 0;
              })
            };
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = {
            total: this.table.selected.length,
            skip: 0,
            value: this.table.selected.map( (item) => {
              return {
                id: item.id,
                status: item.status,
                registrationYear: item.registrationYear,
                registrationPeriod: item.registrationPeriod
              };
            })
          };
        }
      }
    }
    return items;
  }*/

  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        // prepare the items by assigning the new status
        const updateStatuses = this.selectedItems.map((item: any) => {
          return {
            id: item.id,
            status: {
              alternateName: status
            },
            registrationYear: item.registrationYear,
            registrationPeriod: item.registrationPeriod
          };
        });
        // start creating batches of registrations
        // based on a fixed size
        let batchIndex = 0;
        const totalItems = updateStatuses.length;
        do {
          // get next batch (use slice-not splice-to prevent change on original array)
          const updateStatusBatch = updateStatuses.slice(batchIndex, batchIndex + this._batchSize);
          try {
            // update status
            await this._context.model('StudentPeriodRegistrations').save(updateStatusBatch);
            // and update success counter
            result.success += updateStatusBatch.length;
          } catch (err) {
            // log error
            console.error(err);
            // and update error counter
            result.errors += updateStatusBatch.length;
          }
          // update progress
          this.refreshAction.emit({
            progress: Math.floor(((batchIndex + this._batchSize) / totalItems) * 100)
          });
          // and update index
          batchIndex += this._batchSize;
        } while (batchIndex < totalItems);
      })().then(() => {
        // reload table
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

    async openAction() {
      try {
        // validate search filter (on smart selection)
        if (this.table.smartSelect) {
          const filterStatus  = this.search.filter && this.search.filter.status;
          if (filterStatus == null || ['open', 'pending'].includes(filterStatus)) {
            const error = this._translateService.instant('Registrations.OpenActionFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
          if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
              // validate registration year and period filters when on index (all) list
              const yearFilter = this.search.filter && this.search.filter.registrationYear;
              const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
              if (!yearFilter || !periodFilter) {
                const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
                return this._modalService.showDialog(error.title, error.message);
              }
          }
        }
        this._loadingService.showLoading();
        // fetch and filter selected items
        const items = await this.getSelectedItems();
        this.selectedItems = items.filter((item: any) =>
          // consider only items of closed status
          item.status === 'closed' &&
          // and of active students
          item.studentStatus === 'active');
        this._loadingService.hideLoading();
        this._modalService.openModalComponent(AdvancedRowActionComponent, {
          class: 'modal-lg',
          keyboard: false,
          ignoreBackdropClick: true,
          initialState: {
            items: this.selectedItems,
            modalTitle: 'Registrations.OpenAction.Title',
            description: 'Registrations.OpenAction.Description',
            refresh: this.refreshAction,
            execute: this.executeChangeStatusAction('open')
          }
        });
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    }

  async closeAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const filterStatus  = this.search.filter && this.search.filter.status;
        if (filterStatus == null || filterStatus === 'closed') {
          const error = this._translateService.instant('Registrations.CloseActionFilter');
          return this._modalService.showDialog(error.title, error.message);
        }
        if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
          // validate registration year and period filters when on index (all) list
          const yearFilter = this.search.filter && this.search.filter.registrationYear;
          const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
          if (!yearFilter || !periodFilter) {
            const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
        }
      }
      this._loadingService.showLoading();
      // fetch and filter selected items
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) =>
        // consider items of open/pending status
        ['open', 'pending'].includes(item.status));
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.CloseAction.Title',
          description: 'Registrations.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async deleteAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
          // validate registration year and period filters when on index (all) list
          const yearFilter = this.search.filter && this.search.filter.registrationYear;
          const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
          if (!yearFilter || !periodFilter) {
            const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
        }
      }
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter out items with classes or history
      this.selectedItems = items.filter(item => {
        // consider items with no classes
        const totalClasses = item.classes && item.classes.length && item.classes[0].totalClasses;
        // no registration history
        const totalHistory = item.documents && item.documents.length && item.documents[0].totalHistory;
        // and of active students
        const activeStudent = item.studentStatus === 'active';
        return (!totalClasses || totalClasses === 0) && (!totalHistory || totalHistory === 0) && (activeStudent);
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.DeleteAction.Title',
          description: 'Registrations.DeleteAction.Description',
          errorMessage: 'Registrations.DeleteAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeDeleteAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const item = this.selectedItems[index];
            // assign delete state
            Object.defineProperty(item, '$state', {
              configurable: true,
              writable: true,
              enumerable: true,
              value: 4
            });
            // and save
            await this._context.model('StudentPeriodRegistrations').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        // reload table on success / fetchOne is not needed for this action
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async createRegistrationAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter out items with classes or history
      this.selectedItems = items.filter(item => {
        const hasCurrentRegistration = item.registrations && item.registrations.length && item.registrations[0].hasCurrentRegistration;
        return !hasCurrentRegistration || hasCurrentRegistration === 0;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.CreateRegistrationAction.Title',
          description: 'Registrations.CreateRegistrationAction.Description',
          errorMessage: 'Registrations.CreateRegistrationAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCreateRegistrationAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeCreateRegistrationAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        // always get currentYear, currentPeriod from db
        // ignore local cache
        // refetch the department on-action-click
        const department = await this._context.model('LocalDepartments')
          .where('id').equal(this.activeDepartment && this.activeDepartment.id)
          .select('currentYear/id as currentYear', 'currentPeriod/id as currentPeriod')
          .getItem();
        if (!(department && department.currentYear && department.currentPeriod)) {
          return Promise.resolve();
        }
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const item = this.selectedItems[index];
            const newRegistration = {
              student: item.id,
              registrationYear: department.currentYear,
              registrationPeriod: department.currentPeriod,
              registrationDate: new Date(),
              status: {
                alternateName: 'open'
              }
            };
            // recheck if student has registration
            const existingRegistration = await this._context.model('StudentPeriodRegistrations')
              .where('student').equal(newRegistration.student)
              .and('registrationYear').equal(newRegistration.registrationYear)
              .and('registrationPeriod').equal(newRegistration.registrationPeriod)
              .select('id')
              .getItem();
            // and throw error
            if (existingRegistration) {
              throw new Error(this._translateService.instant('Registrations.CreateRegistrationAction.RegistrationExists'));
            }
            // and save
            const registrationResult = await this._context.model(`Students/${item.id}/registrations`).save(newRegistration);
            // catch validation result error
            if (registrationResult && registrationResult.validationResult && registrationResult.validationResult.success === false) {
              throw new Error(registrationResult.validationResult.innerMessage || registrationResult.validationResult.message);
            }
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // select only id for students table
          const selectArguments = [
            'id'
          ];
          if (this.table && this.table.config && this.table.config.model !== 'Students') {
            // push extra select arguments for registration tables
            Array.prototype.push.apply(selectArguments,
              [
                'status/alternateName as status',
                'registrationYear/id as registrationYear',
                'registrationPeriod/id as registrationPeriod',
                'student/studentStatus/alternateName as studentStatus'
              ]);
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected;
        }
      }
    }
    return items;
  }

  async fetchAll(firstLoad: boolean) {
    const lastQuery = this.table && this.table.lastQuery;
    if (lastQuery != null && !firstLoad) {
      // fetch all items for last query (search query)
      // can be used to apply search filters only for students without registration
      return await lastQuery.take(-1).skip(0).getItems();
    }
    // fetch all students, in-sync with table config
    return (await this._context.model('Students')
      .where('studentStatus/alternateName').equal('active')
      .and('department').equal(this.activeDepartment && this.activeDepartment.id)
      // tslint:disable-next-line:max-line-length
      .select('id as id,studyProgram as studyProgramId,studyProgramSpecialty as studyProgramSpecialtyId,studentIdentifier as studentIdentifier,person/familyName as familyName,person/givenName as givenName,studyProgram/abbreviation as studyProgram,studyProgramSpecialty/abbreviation as studyProgramSpecialty,semester as semester,studentStatus/alternateName as studentStatus')
      // tslint:disable-next-line:max-line-length
      .expand('registrations($filter=registrationYear eq $it/student/department/currentYear and registrationPeriod eq $it/student/department/currentPeriod;$select=count(id) as hasCurrentRegistration,student;$groupby=student)')
      .take(-1)
      .getItems());
  }

}
