import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import { AngularDataContext } from '@themost/angular';

export declare interface DegreeTemplate {
    id?: string;
    titleID: number;
    title?: string;
    studyProgramName?: string;
    departmentCode?: string;
    studyLevelCode?: number;
    instituteName?: string;
    publisherName?: string;
    dateCreated?: Date;
    dateModified?: Date;
}

@Injectable()
export class DegreeTemplateService {
    constructor(private context: AngularDataContext, private http: HttpClient) {
        //
    }
    async parse(file: File): Promise<DegreeTemplate[]> {
        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        // get context service headers
        const serviceHeaders = this.context.getService().getHeaders();
        const postUrl = this.context.getService().resolve(`DegreeTemplates/PreImport`);

        const results: any = await this.http.post(postUrl, formData, {
            headers: serviceHeaders,
            responseType: 'json'
        }).toPromise();
        return results;
    }

    async import(items: DegreeTemplate[]) {
        await this.context.model('DegreeTemplates').save(items);
    }

}
